package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.AddressAPI.AddressAPIServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.AddressAPIService.GetAddressInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.AddressAPIService.GetAddressReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetAddressTests {

	@Test
	public void testOperationGetAddressBasicMapping()  {
		AddressAPIServiceDefaultImpl serviceDefaultImpl = new AddressAPIServiceDefaultImpl();
		GetAddressInputParametersDTO inputs = new GetAddressInputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		GetAddressReturnDTO returnValue = serviceDefaultImpl.getAddress(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}